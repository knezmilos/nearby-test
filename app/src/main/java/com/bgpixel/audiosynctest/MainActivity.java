package com.bgpixel.audiosynctest;

import android.Manifest;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AdvertisingOptions;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.DiscoveredEndpointInfo;
import com.google.android.gms.nearby.connection.DiscoveryOptions;
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;

import java.io.IOException;

@RuntimePermissions
public class MainActivity extends AppCompatActivity implements MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener {

    private MediaPlayer mediaPlayer;
    private String enpointId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnCompletionListener(MainActivity.this);
        mediaPlayer.setOnErrorListener(MainActivity.this);

        findViewById(R.id.play).setOnClickListener(v ->
                MainActivityPermissionsDispatcher.onPlayClickedWithPermissionCheck(MainActivity.this));


        findViewById(R.id.server).setOnClickListener(v -> onServerClicked());


        findViewById(R.id.client).setOnClickListener(v -> onClientClicked());
    }

    @NeedsPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
    public void onPlayClicked() {
        if(enpointId != null)
            Nearby.getConnectionsClient(this)
                    .sendPayload(enpointId, Payload.fromBytes("Zdravo, tebra".getBytes()));
        playSound();
    }

    private void playSound() {
        AssetFileDescriptor afd = getResources().openRawResourceFd(R.raw.audio_106);
        try {
            mediaPlayer.setDataSource(
                    afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            afd.close();
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        mediaPlayer.reset();
        return true;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mediaPlayer.reset();
    }



    // -------------------------------------------------------------------------------------- SERVER

    public void onServerClicked() {
        Nearby.getConnectionsClient(this).startAdvertising(
                "Serverko",
                "MOJSERVIS", // ime ovog servisa
                connectionLifecycleCallback,
                new AdvertisingOptions.Builder().setStrategy(Strategy.P2P_CLUSTER).build())
                .addOnSuccessListener(
                        unusedResult -> Toast.makeText(MainActivity.this, "We're advertising!",
                                Toast.LENGTH_LONG).show())
                .addOnFailureListener(
                        e -> Toast.makeText(MainActivity.this,
                                "We were unable to start advertising.", Toast.LENGTH_LONG).show());
    }


    // ------------------------------------------------------------------------------------- KLIJENT

    public void onClientClicked() {
        Nearby.getConnectionsClient(this).startDiscovery(
                "MOJSERVIS", // ime servisa na koje zelimo da se zakacimo
                mEndpointDiscoveryCallback,
                new DiscoveryOptions.Builder().setStrategy(Strategy.P2P_CLUSTER).build())
                .addOnSuccessListener(
                        unusedResult -> Toast.makeText(MainActivity.this, "We're discovering!",
                                Toast.LENGTH_LONG).show())
                .addOnFailureListener(
                        unusedResult -> Toast.makeText(MainActivity.this, "We're unable to start discovering!",
                                Toast.LENGTH_LONG).show());
    }


    private EndpointDiscoveryCallback mEndpointDiscoveryCallback = new EndpointDiscoveryCallback() {
        @Override
        public void onEndpointFound(@NonNull String endpointId,
                                    @NonNull DiscoveredEndpointInfo discoveredEndpointInfo) {
            enpointId = endpointId;
            Nearby.getConnectionsClient(MainActivity.this).requestConnection(
                    "Klijentko", // nase ime
                    endpointId, // ovo je ENDPOINT ID a ne servis ID
                    connectionLifecycleCallback)
                    .addOnSuccessListener(
                            unusedResult -> Toast.makeText(MainActivity.this, "Successfuly requested a connection!",
                                    Toast.LENGTH_LONG).show())
                    .addOnFailureListener(
                            e -> Toast.makeText(MainActivity.this, "Failed to requested a connection!",
                                    Toast.LENGTH_LONG).show());
        }

        @Override
        public void onEndpointLost(@NonNull String s) {
            Toast.makeText(MainActivity.this, "Endpoint lost!", Toast.LENGTH_LONG).show();
        }
    };


    // ---------------------------------------------------------------------------------- ZAJEDNICKO

    private ConnectionLifecycleCallback connectionLifecycleCallback = new ConnectionLifecycleCallback() {
        @Override
        public void onConnectionInitiated(@NonNull String endpointId, @NonNull ConnectionInfo connectionInfo) {
            // Automatically accept the connection
            Nearby.getConnectionsClient(MainActivity.this).acceptConnection(endpointId, mPayloadCallback);
        }

        @Override
        public void onConnectionResult(@NonNull String s, @NonNull ConnectionResolution result) {
            switch (result.getStatus().getStatusCode()) {
                case ConnectionsStatusCodes.STATUS_OK:
                    Toast.makeText(MainActivity.this, "We're connected! Can now start sending and receiving data.",
                            Toast.LENGTH_LONG).show();
                    break;
                case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                    Toast.makeText(MainActivity.this, "The connection was rejected by one or both sides.",
                            Toast.LENGTH_LONG).show();
                    break;
                case ConnectionsStatusCodes.STATUS_ERROR:
                    Toast.makeText(MainActivity.this, "The connection broke before it was able to be accepted.",
                            Toast.LENGTH_LONG).show();
                    break;
            }
        }

        @Override
        public void onDisconnected(@NonNull String s) {
            Toast.makeText(MainActivity.this, "Disconnected", Toast.LENGTH_LONG).show();
        }
    };

    private PayloadCallback mPayloadCallback = new PayloadCallback() {
        @Override
        public void onPayloadReceived(@NonNull String s, @NonNull Payload payload) {
            byte[] bytes = payload.asBytes();
            if(bytes == null) return;
            String string = new String(bytes);
            Toast.makeText(MainActivity.this, "Poruka: " + string, Toast.LENGTH_LONG).show();
            playSound();
        }

        @Override
        public void onPayloadTransferUpdate(@NonNull String s,
                                            @NonNull PayloadTransferUpdate payloadTransferUpdate) {}
    };

}
